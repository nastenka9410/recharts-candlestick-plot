// Core
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../../store';

// Components
import Page from 'components/Page';

export default class App extends Component {
    render () {
        return (
            <Provider store = { store }>
                <Page />
            </Provider>
        );
    }
}
