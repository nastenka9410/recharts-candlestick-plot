//Core
import { takeEvery } from 'redux-saga/effects';

//Instruments
import types from '../../actions/types';
import { fetchDataWorker } from './workers/fetchData';
import { updateDataWorker } from './workers/updateData';

export default Object.freeze({
    * fetchDataWatcher () {
        yield takeEvery(types.FETCH_DATA, fetchDataWorker);
    },
    * updateDataWatcher () {
        yield takeEvery(types.UPDATE_DATA, updateDataWorker);
    },
});
