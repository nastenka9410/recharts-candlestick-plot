// Core
import { combineReducers } from 'redux';

//Instruments
import data from './data';

export default combineReducers({
    data,
});
