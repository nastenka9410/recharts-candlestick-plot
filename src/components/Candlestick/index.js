import React, { Component } from 'react';

export default class Candlestick extends Component {
    calculations = () => {
        const { open, close, high, low } = this.props.payload;
        const { cx, cy } = this.props;

        const condition = open < close; // green

        const candleBodyHeight = Math.abs(open - close) + 8;

        const upperShadow = { x1: cx, x2: cx, y1: cy, y2: cy };
        const candleBody = { x: cx, y: cy, height: candleBodyHeight };
        const lowerShadow = { x1: cx, x2: cx, y1: cy, y2: cy };

        let upperShadowHeight = 0;
        let lowerShadowHeight = 0;
        let color = 'rgb(60,179,113)';

        if (condition) {
            upperShadowHeight = Math.abs(high - close);
            lowerShadowHeight = Math.abs(open - low);
            color = 'rgb(60,179,113)';

        } else if (!condition) {
            upperShadowHeight = Math.abs(high - open);
            lowerShadowHeight = Math.abs(close - low);
            color = 'rgb(220,20,60)';
        }

        upperShadow.y1 = cy + 4;
        upperShadow.y2 = cy + 4 + upperShadowHeight;

        candleBody.x = cx - 4;
        candleBody.y = upperShadow.y2;

        lowerShadow.y1 = candleBody.y + candleBodyHeight;
        lowerShadow.y2 = candleBody.y + candleBodyHeight + lowerShadowHeight;

        return { upperShadow, candleBody, lowerShadow, color };
    }

    render () {
        const { upperShadow, candleBody, lowerShadow, color } = this.calculations();
        const svgStyles = {
            line: {
                stroke:      color,
                strokeWidth: 1,
                fill:        color,
            },
            rect: {
                fill:        color,
                stroke:      color,
                strokeWidth: 1,
            },
        };

        return (
            <svg
                className = 'candlestick'>
                <g>
                    <line
                        style = { svgStyles.line }
                        x1 = { upperShadow.x1 || 0 }
                        x2 = { upperShadow.x2 || 0 }
                        y1 = { upperShadow.y1 || 0 }
                        y2 = { upperShadow.y2 || 0 }
                    />
                    <rect
                        height = { candleBody.height || 0 }
                        style = { svgStyles.rect || 0 }
                        width = { 8 }
                        x = { candleBody.x || 0 }
                        y = { candleBody.y || 0 }
                    />
                    <line
                        style = { svgStyles.line }
                        x1 = { lowerShadow.x1 || 0 }
                        x2 = { lowerShadow.x2 || 0 }
                        y1 = { lowerShadow.y1 || 0 }
                        y2 = { lowerShadow.y2 || 0 }
                    />
                </g>
            </svg>
        );
    }
}
