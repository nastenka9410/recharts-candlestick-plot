// Core
import React, { Component } from 'react';
import { object, any } from 'prop-types';

// Material-ui
import Card from 'material-ui/Card';
import Typography from 'material-ui/Typography';

// Instruments
import Plot from '../Plot';
import Styles from './styles';
import { time } from '../../utils';

export default class Content extends Component {
    static propTypes = {
        actions: object.isRequired,
        data:    any.isRequired,
    };

    static defaultProps = {
        data:    [],
        actions: {
            fetchData:  () => {},
            updateData: () => {},
        },
    };

    componentWillMount () {
        this.props.actions.fetchData();
    }
    componentDidMount () {
        this.refetch = setInterval(this.props.actions.updateData, 15*60*1000);
    }

    componentWillUnmount () {
        clearInterval(this.refetch);
    }

    render () {
        const { data } = this.props;
        const latestData = data.filter((item) => item.mts > time() - 24000000 ? item : null);

        return (
            <div>
                {data ? (<Card className = { Styles.container } ><Plot data = { latestData } /></Card>) : (<Card className = { Styles.container } ><Typography className = { Styles.loading }>Loading...</Typography></Card>)}
            </div>
        );
    }
}
