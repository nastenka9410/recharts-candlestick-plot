// Core
import { List } from 'immutable';

// Instruments
import types from '../../actions/types';

const initialState = List([]);

export default (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_DATA_SUCCEED: {
            return [...state, ...action.payload];
        }
        case types.UPDATE_DATA_SUCCEED: {
            return [...state, ...[action.payload]];
        }
        default:
            return state;
    }
};
