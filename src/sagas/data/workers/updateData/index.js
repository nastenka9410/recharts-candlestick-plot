//Core
import { call, put } from 'redux-saga/effects';

//Instruments
import actions from '../../../../actions';
import { apiLast15 } from '../../../../instruments';
import { message200, message403, formattDataList } from '../../../../utils';

export function* updateDataWorker () {
    try {
        const response = yield call(fetch, `${apiLast15}`, {
            method:  'GET',
            headers: {
                'Accept':       'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        });

        const data = yield call([response, response.json]);

        const dataArray = formattDataList(data);

        if (response.status !== 200) {
            if (response.status === 403) {
                throw new Error(message403);
            }
            throw new Error(message200);
        }

        yield put(actions.updateDataSucceed(dataArray));
    } catch (error) {
        yield put(actions.updateDataFailed(error.message));
    }
}
