//Core
import { call, put } from 'redux-saga/effects';

//Instruments
import actions from '../../../../actions';
import { apiHist15 } from '../../../../instruments';
import { message200, message403, formattDataHist } from '../../../../utils';

export function* fetchDataWorker () {
    try {
        const response = yield call(fetch, `${apiHist15}`, {
            method:  'GET',
            headers: {
                'Accept':       'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        });

        const data = yield call([response, response.json]);

        const dataArray = formattDataHist(data);

        if (response.status !== 200) {
            if (response.status === 403) {
                throw new Error(message403);
            }
            throw new Error(message200);
        }

        yield put(actions.fetchDataSucceed(dataArray));
        yield put(actions.updateData());
    } catch (error) {
        yield put(actions.fetchDataFailed(error.message));
    }
}
