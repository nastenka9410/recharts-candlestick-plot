export const formattDataHist = (data) => {
    let dataArray = [];
    let dataObject = {};

    for (let item in data) {
        const [mts, open, close, high, low, volume] = data[item];

        dataObject[item] = { mts, open, close, high, low, volume };
        dataArray = [...dataArray, dataObject[item]];
    }

    return dataArray;
};

export const formattDataList = (data) => {
    const [mts, open, close, high, low, volume] = data;
    const dataObject = { mts, open, close, high, low, volume };

    return dataObject;
};

export const timeformatter = (mts) => new Date(mts).toLocaleTimeString();
export const message200 = 'ERROR! Response status is not 200!';
export const message403 = 'ERROR! 403 Forbidden – you don\'t have permission to access on this server.';
export const time = () => {
    const date = new Date();

    return date.getTime();
};
