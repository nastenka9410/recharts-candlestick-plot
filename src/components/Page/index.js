// Core
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import action from '../../actions';

// Material-ui
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

// Instruments
import Content from '../../components/Content';
import Styles from './styles';

class Page extends Component {
    static defaultProps = {
        data: {},
    };

    render () {
        const { actions, data } = this.props;

        return (
            <div className = { Styles.page } >
                <Grid container spacing = { 24 }>
                    <Grid item xs = { 12 }>
                        <Typography className = { Styles.title }>BTC / EUR</Typography>
                    </Grid>
                    <Grid item xs = { 12 }>
                        <Content actions = { actions } data = { data } />
                    </Grid>
                </Grid>
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
    data: state.data,
});
const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({ ...action }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Page);
