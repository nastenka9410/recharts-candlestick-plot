// Core
import { all } from 'redux-saga/effects';

//Instruments
import data from './data';

export function* saga () {
    yield all([
        data.fetchDataWatcher(),
        data.updateDataWatcher()
    ]);
}
