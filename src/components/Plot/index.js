// Core
import React, { Component } from 'react';
import {
    ScatterChart,
    Scatter,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer
} from 'recharts';

// Instruments
import Candlestick from '../Candlestick';
import CustomTooltip from '../Tooltip';
import { timeformatter } from '../../utils';

export default class Plot extends Component {
    static defaultProps = {
        data: [],
    };

    formatXAxis = (mts) => timeformatter(mts);

    render () {
        const { data } = this.props;
        const axisStyles = {
            fontFamily:  'sans-serif',
            stroke:      '#1D2A5B',
            type:        'number',
            strokeWidth: '1px',
        };

        return (
            <ResponsiveContainer height = { 550 } minWidth = { 296 }>
                <ScatterChart
                    height = { 550 }
                    margin = { { top: 20, right: 20, bottom: 20, left: 20 } }
                    width = { 700 }>
                    <Tooltip content = { <CustomTooltip /> } />
                    <XAxis
                        dataKey = { 'mts' }
                        domain = { ['dataMin - 126000', 'dataMax + 126000'] }
                        name = 'time'
                        padding = { { left: 2, right: 2 } }
                        tickFormatter = { this.formatXAxis }
                        { ...axisStyles }
                    />
                    <YAxis
                        dataKey = { 'high' }
                        domain = { ['dataMin - 100', 'dataMax + 100'] }
                        name = 'high'
                        { ...axisStyles }
                    />
                    <CartesianGrid stroke = '#1D2A5B' strokeWidth = '1px' />
                    <Legend iconType = 'line' />
                    <Scatter
                        data = { data }
                        name = 'BTC and EUR'
                        shape = { <Candlestick /> }
                    />
                </ScatterChart>
            </ResponsiveContainer>
        );
    }
}
