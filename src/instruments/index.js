// BTC - EUR api
export const apiLast15 =
    'https://api.bitfinex.com/v2/candles/trade:15m:tBTCEUR/last';

export const apiHist15 =
    'https://api.bitfinex.com/v2/candles/trade:15m:tBTCEUR/hist';
