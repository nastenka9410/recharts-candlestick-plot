import React, { Component } from 'react';
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';

//Instruments
import { timeformatter } from '../../utils';

export default class Tooltip extends Component {
    render () {
        const { active } = this.props;
        const { open, close, high, low, volume, mts } = active
            ? this.props.payload[1].payload
            : [];

        return (
            <div>
                {active ? (
                    <Card>
                        <CardContent>
                            <Typography component = 'p'>
                                <b>Time:</b> {timeformatter(mts)}<br />
                                <b>Open:</b> {open}<br />
                                <b>Close:</b> {close}<br />
                                <b>High:</b> {high}<br />
                                <b>Low:</b> {low}<br />
                                <b>Volume:</b> {volume}
                            </Typography>
                        </CardContent>
                    </Card>
                ) : null}
            </div>
        );
    }
}
