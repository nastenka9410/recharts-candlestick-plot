import types from './types';

export default Object.freeze({
    //fetching
    fetchData: () => ({
        type: types.FETCH_DATA,
    }),
    fetchDataSucceed: (data) => ({
        type:    types.FETCH_DATA_SUCCEED,
        payload: data,
    }),
    fetchDataFailed: (error) => ({
        type:    types.FETCH_DATA_FAILED,
        payload: error,
        error:   true,
    }),
    //updating
    updateData: () => ({
        type: types.UPDATE_DATA,
    }),
    updateDataSucceed: (candle) => ({
        type:    types.UPDATE_DATA_SUCCEED,
        payload: candle,
    }),
    updateDataFailed: (error) => ({
        type:    types.UPDATE_DATA_FAILED,
        payload: error,
        error:   true,
    }),
});
